<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        // 'css/font-face.css',
        // 'css/theme.css',
        // 'vendor/slick/slick.css',
        // 'vendor/select2/select2.min.css',
        // 'vendor/perfect-scrollbar/perfect-scrollbar.css',
        "assets-nice/img/favicon.png",
        "assets-nice/img/apple-touch-icon.png",
        "https://fonts.gstatic.com",
        "https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i",
        "assets-nice/vendor/bootstrap/css/bootstrap.min.css",
        "assets-nice/vendor/bootstrap-icons/bootstrap-icons.css",
        "assets-nice/vendor/boxicons/css/boxicons.min.css",
        "assets-nice/vendor/quill/quill.snow.css",
        "assets-nice/vendor/quill/quill.bubble.css",
        "assets-nice/vendor/remixicon/remixicon.css",
        "assets-nice/vendor/simple-datatables/style.css",
    ];
    public $js = [

        "assets-nice/vendor/apexcharts/apexcharts.min.js",
        "assets-nice/vendor/bootstrap/js/bootstrap.bundle.min.js",
        "assets-nice/vendor/chart.js/chart.min.js",
        "assets-nice/vendor/echarts/echarts.min.js",
        "assets-nice/vendor/quill/quill.min.js",
        "assets-nice/vendor/simple-datatables/simple-datatables.js",
        "assets-nice/vendor/tinymce/tinymce.min.js",
        "assets-nice/vendor/php-email-form/validate.js",
        "assets-nice/js/main.js",
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap5\BootstrapAsset'
    ];
}
