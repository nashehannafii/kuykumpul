<?php

namespace app\controllers;

use app\models\Spk;
use app\models\SpkSearch;
use app\models\Weight;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SpkController implements the CRUD actions for Spk model.
 */
class SpkController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Spk models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new SpkSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionBarang()
    {
        $searchModel = new SpkSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('barang', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Spk model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Spk model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $is_bobot = Weight::find()->one();
        if (!isset($is_bobot)) {
            // if ($is_bobot == 0) {
            return $this->redirect(['tobobot']);
        }
        $model = new Spk();

        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {

                $is_bobot = Weight::find()->one();

                // echo '<pre>';print_r($is_bobot);exit;

                $W_keuntungan = $is_bobot['keuntungan'];
                $W_penjualan = $is_bobot['penjualan'];
                $W_harga = $is_bobot['harga'];

                $C_keuntungan = $W_keuntungan * $model->keuntungan;
                $C_penjualan = $W_penjualan * $model->penjualan;
                $C_harga = $W_harga * $model->harga;

                $res = $C_keuntungan + $C_penjualan + $C_harga;
                $res = $res / 3;

                $model->nilai_hitung = $res;
                // echo '<pre>';print_r($model);exit;
                $model->save();
                return $this->redirect(['barang']);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Spk model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post())) {
            $is_bobot = Weight::find()->one();

            // echo '<pre>';print_r($is_bobot);exit;

            $W_keuntungan = $is_bobot['keuntungan'];
            $W_penjualan = $is_bobot['penjualan'];
            $W_harga = $is_bobot['harga'];

            $C_keuntungan = $W_keuntungan * $model->keuntungan;
            $C_penjualan = $W_penjualan * $model->penjualan;
            $C_harga = $W_harga * $model->harga;

            $res = $C_keuntungan + $C_penjualan + $C_harga;
            $res = $res / 3;

            $model->nilai_hitung = $res;

            $model->save();
            return $this->redirect(['barang']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Spk model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionDeletedata($id)
    {
        // echo '<pre>';print_r($id);exit;
        $this->findModel($id)->delete();

        return $this->redirect(['barang']);
    }

    public function actionRekomendasi()
    {
        // $data_barang = Spk::

        return $this->render('rekomendasi', []);
    }

    public function actionTobobot()
    {
        // $data_barang = Spk::

        return $this->render('tobobot');
    }



    /**
     * Finds the Spk model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Spk the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Spk::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
