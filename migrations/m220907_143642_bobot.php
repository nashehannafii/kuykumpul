<?php

use yii\db\Schema;
use yii\db\Migration;

class m220907_143642_bobot extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%bobot}}',
            [
                'id' => $this->primaryKey(11),
                'jenis_bobot' => $this->string(100)->notNull(),
                'bobot' => $this->integer(11)->notNull(),
                'keterangan_bobot' => $this->string(255)->notNull(),
            ],
            $tableOptions
        );

        $this->insert('bobot', [
            'jenis_bobot' => 'Harga',
            'bobot' => '10',
            'keterangan_bobot'=>'<= Rp. 499,00'
        ]);
        $this->insert('bobot', [
            'jenis_bobot' => 'Harga',
            'bobot' => '9',
            'keterangan_bobot'=>'Rp 1.999,00 - Rp. 500,00'
        ]);
        $this->insert('bobot', [
            'jenis_bobot' => 'Harga',
            'bobot' => '8',
            'keterangan_bobot'=>'Rp 4.999,00 - Rp. 2.000,00'
        ]);
        $this->insert('bobot', [
            'jenis_bobot' => 'Harga',
            'bobot' => '7',
            'keterangan_bobot'=>'Rp 9.999,00 - Rp. 5.000,00'
        ]);
        $this->insert('bobot', [
            'jenis_bobot' => 'Harga',
            'bobot' => '6',
            'keterangan_bobot'=>'Rp 15.999,00 - Rp. 10.000,00'
        ]);
        $this->insert('bobot', [
            'jenis_bobot' => 'Harga',
            'bobot' => '5',
            'keterangan_bobot'=>'Rp 19.999,00 - Rp. 15.000,00'
        ]);
        $this->insert('bobot', [
            'jenis_bobot' => 'Harga',
            'bobot' => '4',
            'keterangan_bobot'=>'Rp 24.999,00 - Rp. 20.000,00'
        ]);
        $this->insert('bobot', [
            'jenis_bobot' => 'Harga',
            'bobot' => '3',
            'keterangan_bobot'=>'Rp 29.999,00 - Rp. 25.000,00'
        ]);
        $this->insert('bobot', [
            'jenis_bobot' => 'Harga',
            'bobot' => '2',
            'keterangan_bobot'=>'Rp 34.999,00 - Rp. 30.000,00'
        ]);
        $this->insert('bobot', [
            'jenis_bobot' => 'Harga',
            'bobot' => '1',
            'keterangan_bobot'=>'>= Rp. 35.000,00'
        ]);




        $this->insert('bobot', [
            'jenis_bobot' => 'Penjualan',
            'bobot' => '1',
            'keterangan_bobot'=>'<= 5'
        ]);
        $this->insert('bobot', [
            'jenis_bobot' => 'Penjualan',
            'bobot' => '2',
            'keterangan_bobot'=>'6 - 11'
        ]);
        $this->insert('bobot', [
            'jenis_bobot' => 'Penjualan',
            'bobot' => '3',
            'keterangan_bobot'=>'12 - 19'
        ]);
        $this->insert('bobot', [
            'jenis_bobot' => 'Penjualan',
            'bobot' => '4',
            'keterangan_bobot'=>'20 - 31'
        ]);
        $this->insert('bobot', [
            'jenis_bobot' => 'Penjualan',
            'bobot' => '5',
            'keterangan_bobot'=>'32 - 39'
        ]);
        $this->insert('bobot', [
            'jenis_bobot' => 'Penjualan',
            'bobot' => '6',
            'keterangan_bobot'=>'40 - 51'
        ]);
        $this->insert('bobot', [
            'jenis_bobot' => 'Penjualan',
            'bobot' => '7',
            'keterangan_bobot'=>'52 - 59'
        ]);
        $this->insert('bobot', [
            'jenis_bobot' => 'Penjualan',
            'bobot' => '8',
            'keterangan_bobot'=>'60 - 71'
        ]);
        $this->insert('bobot', [
            'jenis_bobot' => 'Penjualan',
            'bobot' => '9',
            'keterangan_bobot'=>'79 - 72'
        ]);
        $this->insert('bobot', [
            'jenis_bobot' => 'Penjualan',
            'bobot' => '10',
            'keterangan_bobot'=>'>= 80'
        ]);



        $this->insert('bobot', [
            'jenis_bobot' => 'Keuntungan',
            'bobot' => '1',
            'keterangan_bobot'=>'<= Rp. 199,00'
        ]);
        $this->insert('bobot', [
            'jenis_bobot' => 'Keuntungan',
            'bobot' => '2',
            'keterangan_bobot'=>'Rp 499,00 - Rp. 200,00'
        ]);
        $this->insert('bobot', [
            'jenis_bobot' => 'Keuntungan',
            'bobot' => '3',
            'keterangan_bobot'=>'Rp 999,00 - Rp. 500,00'
        ]);
        $this->insert('bobot', [
            'jenis_bobot' => 'Keuntungan',
            'bobot' => '4',
            'keterangan_bobot'=>'Rp 1.199,00 - Rp. 1.000,00'
        ]);
        $this->insert('bobot', [
            'jenis_bobot' => 'Keuntungan',
            'bobot' => '5',
            'keterangan_bobot'=>'Rp 1.499,00 - Rp. 1.200,00'
        ]);
        $this->insert('bobot', [
            'jenis_bobot' => 'Keuntungan',
            'bobot' => '6',
            'keterangan_bobot'=>'Rp 1.999,00 - Rp. 1.500,00'
        ]);
        $this->insert('bobot', [
            'jenis_bobot' => 'Keuntungan',
            'bobot' => '7',
            'keterangan_bobot'=>'Rp 2.499,00 - Rp. 2.000,00'
        ]);
        $this->insert('bobot', [
            'jenis_bobot' => 'Keuntungan',
            'bobot' => '8',
            'keterangan_bobot'=>'Rp 3.499,00 - Rp. 2.500,00'
        ]);
        $this->insert('bobot', [
            'jenis_bobot' => 'Keuntungan',
            'bobot' => '9',
            'keterangan_bobot'=>'Rp 4.999,00 - Rp. 3.500,00'
        ]);
        $this->insert('bobot', [
            'jenis_bobot' => 'Keuntungan',
            'bobot' => '10',
            'keterangan_bobot'=>'>= Rp. 5.000,00'
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%bobot}}');
    }
}
