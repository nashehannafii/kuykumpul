<?php

use yii\db\Schema;
use yii\db\Migration;

class m220907_160128_weight extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%weight}}',
            [
                'id'=> $this->integer(11)->null()->defaultValue(1),
                'keuntungan'=> $this->double()->notNull(),
                'penjualan'=> $this->double()->notNull(),
                'harga'=> $this->double()->notNull(),
            ],$tableOptions
        );

    }

    public function safeDown()
    {
        $this->dropTable('{{%weight}}');
    }
}
