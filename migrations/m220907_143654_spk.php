<?php

use yii\db\Schema;
use yii\db\Migration;

class m220907_143654_spk extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%spk}}',
            [
                'id'=> $this->primaryKey(11),
                'nama_barang'=> $this->string(100)->notNull(),
                'keuntungan'=> $this->integer(11)->notNull(),
                'penjualan'=> $this->integer(11)->notNull(),
                'harga'=> $this->integer(11)->notNull(),
                'nilai_hitung'=> $this->double()->notNull(),
            ],$tableOptions
        );

    }

    public function safeDown()
    {
        $this->dropTable('{{%spk}}');
    }
}
