<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "weight".
 *
 * @property int|null $id
 * @property float $keuntungan
 * @property float $penjualan
 * @property float $harga
 */
class Weight extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'weight';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['keuntungan', 'penjualan', 'harga'], 'required'],
            [['keuntungan', 'penjualan', 'harga'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'keuntungan' => 'Keuntungan',
            'penjualan' => 'Penjualan',
            'harga' => 'Harga',
        ];
    }
}
