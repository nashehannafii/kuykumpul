<?php

/** @var yii\web\View $this */

$this->title = 'My Yii Application';
?>

<div class="row">
    <div class="col-md-12">
        <div class="copyright">

            <h3>Anda belum mengisi bobot nilai, mohon mengisi bobot terlebih dahulu</h3><br>
            <a href="/weight/create" class="btn btn-success"> Isi Bobot</a>

        </div>
    </div>
</div>