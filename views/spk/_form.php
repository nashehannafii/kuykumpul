<?php

use app\models\Bobot;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Spk $model */
/** @var yii\widgets\ActiveForm $form */

$list_keuntungan = Bobot::find()->where([
    'jenis_bobot' => 'Keuntungan',
])->orderBy(['bobot' => SORT_ASC])->all();

$list_penjualan = Bobot::find()->where([
    'jenis_bobot' => 'Penjualan',
])->orderBy(['bobot' => SORT_ASC])->all();

$list_harga = Bobot::find()->where([
    'jenis_bobot' => 'Harga',
])->orderBy(['bobot' => SORT_ASC])->all();

$list_keuntungan = ArrayHelper::map($list_keuntungan, 'bobot', 'keterangan_bobot');
$list_penjualan = ArrayHelper::map($list_penjualan, 'bobot', 'keterangan_bobot');
$list_harga = ArrayHelper::map($list_harga, 'bobot', 'keterangan_bobot');

?>

<div class="spk-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nama_barang')->textInput(['maxlength' => true,'required'=>'required']) ?>

    <?= $form->field($model, 'keuntungan')->dropDownList($list_keuntungan, ['class' => 'form-control', 'prompt' => '- Pilih Bobot Keuntungan -','required'=>'required']) ?>

    <?= $form->field($model, 'penjualan')->dropDownList($list_penjualan, ['class' => 'form-control', 'prompt' => '- Pilih Bobot Penjualan -','required'=>'required']) ?>

    <?= $form->field($model, 'harga')->dropDownList($list_harga, ['class' => 'form-control', 'prompt' => '- Pilih Bobot Harga -','required'=>'required']) ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>