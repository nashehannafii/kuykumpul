<?php

use app\models\Bobot;
use app\models\Spk;
use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\SpkSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Table Data Barang';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="col-lg-12">
    <h1><?= Html::encode($this->title) ?></h1>
    <br>
    <?php

    $spk = Spk::find()->all();
    if (isset($spk)) {
    ?>
        <a href="/spk/create" class="btn btn-success"><i class="fas fa-plus"></i> Tambah Data</a>
    <?php
    }
    ?>
    <br>
    <br>
    <div class="table-responsive table--no-card m-b-30">
        <table class="table table-borderless table-striped table-earning">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Jenis Barang</th>
                    <th>Keuntungan</th>
                    <th>Penjualan</th>
                    <th>Harga</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 1;
                // echo '<pre>';print_r($spk);exit;
                if ($spk == null) :
                ?>
                    <tr>
                        <td>-</td>
                        <td class="text-left">-</td>
                        <td>-</td>
                        <td class="text-left">-</td>
                        <td class="text-left">-</td>
                        <td class="text-left">-</td>
                    </tr>
                <?php
                endif;
                foreach ($spk as $key) :

                    $keuntungan = Bobot::find()->where([
                        'jenis_bobot' => 'Keuntungan',
                        'bobot' => $key->keuntungan
                    ])->one();
                    $penjualan = Bobot::find()->where([
                        'jenis_bobot' => 'Penjualan',
                        'bobot' => $key->penjualan
                    ])->one();
                    $harga = Bobot::find()->where([
                        'jenis_bobot' => 'Harga',
                        'bobot' => $key->harga
                    ])->one();
                ?>

                    <tr>
                        <td><?= $i ?></td>
                        <td class="text-left"><?= $key->nama_barang ?></td>
                        <td><?= $keuntungan->keterangan_bobot ?></td>
                        <td><?= $penjualan->keterangan_bobot ?></td>
                        <td><?= $harga->keterangan_bobot ?></td>
                        <td>
                            <?= Html::a(' ', ['update', 'id' => $key->id], ['class' => 'fas fa-wrench']) ?>
                            <?= Html::a(' ', ['deletedata', 'id' => $key->id], ['class' => 'fas fa-trash']) ?>
                        </td>
                    </tr>

                <?php

                    $i++;
                endforeach; ?>

            </tbody>
        </table>
    </div>
</div>