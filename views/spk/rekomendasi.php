<?php

use app\models\Bobot;
use app\models\Spk;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\SpkSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Hasil Perhitungan Rekomendasi';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="col-lg-12">
<h1><?= Html::encode($this->title) ?></h1>
<br>
    <div class="table-responsive table--no-card m-b-30">
        <table class="table table-borderless table-striped table-earning">
            <thead>
                <tr>
                    <th class="text-center">Rank</th>
                    <th class="text-center">Jenis Barang</th>
                    <th class="text-center">Nilai Akhir</th>
                    <!-- <th>Bobot</th> -->
                </tr>
            </thead>
            <tbody>
                <?php
                $spk = Spk::find()->orderBy(['nilai_hitung' => SORT_DESC])->all();
                $i = 1;
                // echo '<pre>';print_r($spk);exit;
                if ($spk == null) :
                ?>
                    <tr>
                        <td class="text-center">-</td>
                        <td class="text-center">-</td>
                        <td class="text-center">-</td>
                    </tr>
                <?php
                endif;
                foreach ($spk as $key) :
                ?>

                    <tr>
                        <td class="text-center"><?= $i ?></td>
                        <td class="text-center"><?= $key->nama_barang ?></td>
                        <td class="text-center"><?= $key->nilai_hitung ?></td>
                    </tr>

                <?php
                    $i++;
                endforeach; ?>

            </tbody>
        </table>
    </div>
</div>
