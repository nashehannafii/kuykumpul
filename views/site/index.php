<?php

/** @var yii\web\View $this */

$this->title = 'My Yii Application';
?>
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-12">
                    <div class="copyright">
                    
                        <h1>Selamat Datang di <br><br><i class="fas fa-shopping-cart"></i> MyWarung WebApp</h1><br>
                        <h3>Sistem Informasi Penunjang Keputusan <br> Menggunakan Metode Multifactor Evalution Proccess</h3>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>