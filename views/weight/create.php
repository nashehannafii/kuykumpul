<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Weight $model */

$this->title = 'Create Weight';
$this->params['breadcrumbs'][] = ['label' => 'Weights', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="weight-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <br>
    <h3>Masukkan Nilai Bobot Antara 1 - 10, Semakin tinggi = Semakin penting</h3>
    <br>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>