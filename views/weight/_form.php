<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Weight $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="weight-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'keuntungan')->dropDownList(['1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6','7'=>'7','8'=>'8','9'=>'9','10'=>'10'],['class'=>'form-control','maxlength' => true,'required'=>'required', 'prompt' => '- Pilih Bobot -']) ?>
    <?= $form->field($model, 'penjualan')->dropDownList(['1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6','7'=>'7','8'=>'8','9'=>'9','10'=>'10'],['class'=>'form-control','maxlength' => true,'required'=>'required', 'prompt' => '- Pilih Bobot -']) ?>
    <?= $form->field($model, 'harga')->dropDownList(['1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6','7'=>'7','8'=>'8','9'=>'9','10'=>'10'],['class'=>'form-control','maxlength' => true,'required'=>'required', 'prompt' => '- Pilih Bobot -']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
