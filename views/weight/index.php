<?php

use app\models\Weight;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\WeightSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Bobot Nilai';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="col-lg-12">
<h1><?= Html::encode($this->title) ?></h1>
<br>

<br>
<br>
    <div class="table-responsive table--no-card m-b-30">
        <table class="table table-borderless table-striped table-earning">
            <thead>
                <tr>
                    <th class="text-center">Keuntungan</th>
                    <th class="text-center">Penjualan</th>
                    <th class="text-center">Harga</th>
                    <th class="text-center">Aksi</th>
                    <!-- <th>Bobot</th> -->
                </tr>
            </thead>
            <tbody>
                <?php
                $spk = Weight::find()->all();
                // echo '<pre>';print_r($spk);exit;
                if ($spk == null) :
                    ?>
                    <tr>
                        <td class="text-center">-</td>
                        <td class="text-center">-</td>
                        <td class="text-center">-</td>
                    </tr>
                <?php
                endif;
                foreach ($spk as $key) :
                    $total = $key->keuntungan + $key->penjualan + $key->harga;
                    // echo '<pre>';print_r($key);exit;
                    $keuntungan = $key->keuntungan;
                    $penjualan = $key->penjualan;
                    $harga = $key->harga;
            
                    $total = $keuntungan + $penjualan + $harga;
            
                    $keuntungan = $keuntungan / $total;
                    $penjualan = $penjualan / $total;
                    $harga = $harga / $total;
                ?>

                    <tr>
                        <td class="text-center"><?= $keuntungan; ?> : 1</td>
                        <td class="text-center"><?= $penjualan ?> : 1</td>
                        <td class="text-center"><?= $harga ?> : 1</td>
                        <td class="text-center">
                            <?= Html::a('', ['update', 'id' => $key->id], ['class' => 'fas fa-wrench']) ?>
                        </td>
                    </tr>

                <?php
                
                endforeach; ?>

            </tbody>
        </table>
    </div>
</div>

