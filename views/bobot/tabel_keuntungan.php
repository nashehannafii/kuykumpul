<?php

use app\models\Bobot;
use yii\helpers\Html;

?>

<div class="col-lg-12">
    <div class="table-responsive table--no-card m-b-30">
        <table class="table table-borderless table-striped table-earning">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Jenis Kategori</th>
                    <th>Bobot</th>
                    <th class="text-left">Keterangan</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $tabel_keuntungan = Bobot::find()->where([
                    'jenis_bobot' => 'Keuntungan',
                ])->orderBy(['bobot'=>SORT_ASC])->all();
                $i = 1;
                if ($tabel_keuntungan == null) :
                ?>
                    <tr>
                        <td>-</td>
                        <td class="text-left">-</td>
                        <td>-</td>
                        <td class="text-left">-</td>
                        <td>-</td>
                    </tr>
                <?php
                endif;
                foreach ($tabel_keuntungan as $key) :
                ?>

                    <tr>
                        <td><?= $i ?></td>
                        <td class="text-left"><?= $key->jenis_bobot ?></td>
                        <td><?= $key->bobot ?></td>
                        <td class="text-left"><?= $key->keterangan_bobot ?></td>
                        <td class="text-left">
                            <?= Html::a(' ', ['update', 'id' => $key->id], ['class' => 'fas fa-wrench']) ?>
                        </td>
                    </tr>

                <?php
                    $i++;
                endforeach; ?>

            </tbody>
        </table>
    </div>
</div>