<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <h4>Tabel Range Nilai</h4>
        </div>
        <div class="card-body">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Keuntungan</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Penjualan</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Harga</a>
                </li>
            </ul><br>
            <div class="tab-content pl-3 p-1" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <h3>Tabel Keuntungan</h3><br>
                    <?php $this->beginContent("@app/views/bobot/tabel_keuntungan.php") ?>
                    <?php $this->endContent() ?>
                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <h3>Tabel Penjualan</h3><br>
                    <?php $this->beginContent("@app/views/bobot/tabel_penjualan.php") ?>
                    <?php $this->endContent() ?>
                </div>
                <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                    <h3>Tabel Harga</h3><br>
                    <?php $this->beginContent("@app/views/bobot/tabel_harga.php") ?>
                    <?php $this->endContent() ?>
                </div>
            </div>
        </div>
    </div>
</div>