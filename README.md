<p align="center">
    <a href="https://github.com/yiisoft" target="_blank">
        <img src="https://avatars0.githubusercontent.com/u/993323" height="100px">
    </a>
    <h1 align="center">MyWarung App - Yii 2 Basic</h1>
    <br>
</p>

MyWarung App is Powered by Yii 2 Basic Project, Cause Yii2 Basic Template is a skeleton [Yii 2](http://www.yiiframework.com/) application best for
rapidly creating small projects.

The template contains the basic features including user login/logout and a contact page, but i focussed by SPK for this site.
It includes all commonly used configurations that would allow you to focus on adding new
features to your application


DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources




INSTALLATION
------------

### Install via Composer

Just follow my note.

1. Installed `git`, `Composer version 2`, `php8.1`, `Apache`, and `Mysql` on your computer.

2. Download source code using the following command :

~~~
git clone https://gitlab.com/nashehannafii/my-warung.git
~~~

3. Entering file :

~~~
cd mywarung
~~~

4. Install require composer source code using the following command :

~~~
composer install
~~~

5. Create database on your computer.

6. Edit the file `config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=db_name',
    'username' => 'db_user',
    'password' => 'db_password',
    'charset' => 'utf8',
];
```

7. Running migration table using the following command :

~~~
php yii migrate/fresh
~~~


8. Running mywarung App :

~~~
php yii serve
~~~

9. Open on your browser :

~~~
http://localhost:8080
~~~


# UNIDA GONTOR
